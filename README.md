# Temperature and Rainfall in Norway

![](scatterplot.png)


<img src="map.png" alt="map of norway" height="600">

* Made with ggplot2

* Inspired by https://twitter.com/dataandme/status/932610025691983872

* Data from MET Norway: http://eklima.met.no/
